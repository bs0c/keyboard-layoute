#include QMK_KEYBOARD_H
#include "./config.h"

/*
 * TODO
 * Создать клавиши MsgQ MsgC которые будут переключать на слои Qwerty, Colmak и переключать обратно после нажатия Enter
 */

#define SPC_FN LT(_FN, KC_SPC)
#define ALT_ESC LALT_T(KC_ESC)
#define ALT_ENT LALT_T(KC_ENT)
#define QWERTY TO(_QWERTY)
#define COLEMAK TO(_COLEMAK)
#define GAMNUM TO(_GAMNUM)

enum layer_names {
    _QWERTY,
    _COLEMAK,
    _FN,
    _GAMNUM
};

const uint16_t PROGMEM keymaps[][MATRIX_ROWS][MATRIX_COLS] = {

    /* Qwerty layer
     * ,-----------------------------------------.                ,-----------------------------------------.
     * |   1  |   2  |   3  |   4  |   5  |   6  |                |   7  |   8  |   9  |   0  |   -  |   =  |
     * |------+------+------+------+------+------|                |------+------+------+------+------+------|
     * | Tab  |   Q  |   W  |   E  |   R  |   T  |                |   Y  |   U  |   I  |   O  |   P  |   [  |
     * |------+------+------+------+------+------|                |------+------+------+------+------+------|
     * | Caps |   A  |   S  |   D  |   F  |   G  |,------.,------.|   H  |   J  |   K  |   L  |   ;  |   '  |
     * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
     * | Shft |   Z  |   X  |   C  |   V  |   B  ||AltEsc||AltEnt||   N  |   M  |   ,  |   .  |   /  | Shft |
     * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
     * |      |      |COLEMK| Menu | Ctrl |SpcFN |`------'`------'|SpcFN | Ctrl | Menu |QWERTY|      |      |
     * `-----------------------------------------'                `-----------------------------------------'
     */

    [_QWERTY] = LAYOUT(
      KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,                     KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,
      KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,    KC_T,                     KC_Y,    KC_U,    KC_I,    KC_O,    KC_P,    KC_LBRC,
      KC_BSPC, KC_A,    KC_S,    KC_D,    KC_F,    KC_G,                     KC_H,    KC_J,    KC_K,    KC_L,    KC_SCLN, KC_QUOT,
      KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,                     KC_N,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,
      KC_NO,   KC_NO,   COLEMAK, KC_LGUI, KC_LCTL, SPC_FN, ALT_ESC, ALT_ENT, SPC_FN,  KC_RCTL, KC_RGUI, QWERTY,  KC_NO,   KC_NO
    ),

    /* Colemak layer
    * ,-----------------------------------------.                ,-----------------------------------------.
    * |   1  |   2  |   3  |   4  |   5  |   6  |                |   7  |   8  |   9  |   0  |   -  |  =   |
    * |------+------+------+------+------+------|                |------+------+------+------+------+------|
    * |  Tab |   Q  |   W  |   F  |   P  |   G  |                |   J  |   L  |   U  |   Y  |   ;  |   [  |
    * |------+------+------+------+------+------|                |------+------+------+------+------+------|
    * | Bksp |   A  |   R  |   S  |   T  |   D  |,------.,------.|   H  |   N  |   E  |   I  |   O  |   "  |
    * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
    * | Shft |   Z  |   X  |   C  |   V  |   B  ||AltEsc||AltEnt||   N  |   M  |   ,  |   .  |   /  | Shft |
    * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
    * |      |      |COLEMK| Menu | Ctrl |SpcFN |`------'`------'|SpcFN | Ctrl | Menu |QWERTY|      |      |
    * `-----------------------------------------'                `-----------------------------------------'
    */

    [_COLEMAK] = LAYOUT(
      KC_1,    KC_2,    KC_3,    KC_4,    KC_5,    KC_6,                     KC_7,    KC_8,    KC_9,    KC_0,    KC_MINS, KC_EQL,
      KC_TAB,  KC_Q,    KC_W,    KC_F,    KC_P,    KC_G,                     KC_J,    KC_L,    KC_U,    KC_Y,    KC_SCLN, KC_LBRC,
      KC_BSPC, KC_A,    KC_R,    KC_S,    KC_T,    KC_D,                     KC_H,    KC_N,    KC_E,    KC_I,    KC_O,    KC_QUOT,
      KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,    KC_B,                     KC_K,    KC_M,    KC_COMM, KC_DOT,  KC_SLSH, KC_RSFT,
      KC_NO,   KC_NO,   COLEMAK, KC_LGUI, KC_LCTL, SPC_FN, ALT_ESC, ALT_ENT, SPC_FN,  KC_RCTL, KC_RGUI, QWERTY,  KC_NO,   KC_NO
    ),

    /* FN layer
     * ,-----------------------------------------.                ,-----------------------------------------.
     * |  F1  |  F2  |  F3  |  F4  |  F5  |  F6  |                |  F7  |  F8  |  F9  | F10  | F11  | F12  |
     * |------+------+------+------+------+------|                |------+------+------+------+------+------|
     * | PrScr|      |      |      |   `  |   ~  |                |      | Home | PgUp | Right|      |      |
     * |------+------+------+------+------+------|                |------+------+------+------+------+------|
     * |  Del |   [  |   ]  |   {  |   }  | PgDn |,------.,------.| Left | Down |  Up  |      |      |      |
     * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
     * | Lock |      |      |      |      |      ||      ||      ||      |  End |      |  |   |  \   |      |
     * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
     * |      |      |      |      |      |      |`------'`------'|      |      |      |      |      |      |
     * `-----------------------------------------'                `-----------------------------------------'
     */

    [_FN] = LAYOUT(
      KC_F1,   KC_F2,   KC_F3,   KC_F4,   KC_F5,   KC_F6,                     KC_F7,   KC_F8,   KC_F9,   KC_F10,  KC_F11,  KC_F12,
      KC_PSCR, KC_NO,   KC_NO,   KC_NO,   KC_GRV,  KC_TILD,                   KC_NO,   KC_HOME, KC_PGUP, KC_RGHT, KC_NO,   KC_NO,
      KC_DEL,  KC_LBRC, KC_RBRC, KC_LCBR, KC_RCBR, KC_PGDN,                   KC_LEFT, KC_DOWN, KC_UP,   KC_NO,   KC_NO,   KC_NO,
      KC_LOCK, KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,                     KC_NO,   KC_END,  KC_NO,   KC_PIPE, KC_BSLS, KC_NO,
      KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,    KC_NO,  KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO,   KC_NO
    ),

    /* Game and Numpad layer
     * ,-----------------------------------------.                ,-----------------------------------------.
     * |  Esc |   1  |   2  |   3  |   4  |   5  |                |      |      |   /  |   *  |   -  |      |
     * |------+------+------+------+------+------|                |------+------+------+------+------+------|
     * | MsgQ | Tab  |   Q  |   W  |   E  |   R  |                |      |   7  |   8  |   9  |   +  |      |
     * |------+------+------+------+------+------|                |------+------+------+------+------+------|
     * | MsqC | Bksp |   A  |   S  |   D  |   F  |,------.,------.|      |   4  |   5  |   6  | Enter|      |
     * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
     * | Menu | Shift|   Z  |   X  |   C  |   V  || Enter||      ||      |   1  |   2  |   3  |      |      |
     * |------+------+------+------+------+------||      ||      ||------+------+------+------+------+------|
     * |      |COLEMK|      |  Alt | Space| Ctrl |`------'`------'|      |   0  |   ,  |   .  |QWERTY|      |
     * `-----------------------------------------'                `-----------------------------------------'
     */

  [_GAMNUM] = LAYOUT(
    KC_ESC,  KC_1,    KC_2,    KC_3,    KC_4,    KC_5,                      KC_NO,   KC_NO,   KC_SLSH, KC_ASTR, KC_MINS, KC_NO,
    KC_NO,   KC_TAB,  KC_Q,    KC_W,    KC_E,    KC_R,                      KC_NO,   KC_7,    KC_8,    KC_9,    KC_PLUS, KC_NO,
    KC_NO,   KC_BSPC, KC_A,    KC_S,    KC_D,    KC_F,                      KC_NO,   KC_4,    KC_5,    KC_6,    KC_ENT,  KC_NO,
    KC_LGUI, KC_LSFT, KC_Z,    KC_X,    KC_C,    KC_V,                      KC_NO,   KC_1,    KC_2,    KC_3,    KC_NO,   KC_NO,
    KC_NO,   COLEMAK, KC_NO,   KC_LALT, KC_SPC,  KC_LCTL,  KC_ENT,  KC_NO,  KC_NO,   KC_0,    KC_COMM, KC_DOT,  QWERTY,  KC_NO
  ),

};
