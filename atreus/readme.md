Atreus
===

A 60 key variant of the Atreus keyboard.

Keyboard Maintainer:
Hardware Supported:
Hardware Availability:

Make example for this keyboard (after setting up your build environment):

    make atreus60:default

See [build environment setup](https://docs.qmk.fm/#/getting_started_build_tools) then the [make instructions](https://docs.qmk.fm/#/getting_started_make_guide) for more information.
