source := $(PWD)
qmk_git := $(HOME)/qmk_firmware
qmk_keyboard := $(qmk_git)/keyboards
qmk := $(HOME)/.local/bin/qmk
user := bs0c
file_atreus = atreus62.c atreus62.h config.h
SHELL := /bin/bash

.PHONY: help
help:
	@ echo 'usage: make [ help | check | dactyl | iris | atreus | clean]'
	@ echo 'Make and write firmware to keyboard. You need install git, qmk_git and pip. QMK place by default in $$HOME'

.PHONY: iris
iris:
	-ln -s $(source)/$@ $(qmk_keyboard)/keebio/iris/keymaps/$(user)
	$(qmk) compile -kb $@:rev2 -km $(user)
	-rm -r $(qmk_keyboard)/keebio/iris/keymaps/$(user)

.PHONY: dactyl
dactyl:
	-ln -s $(source)/$@ $(qmk_keyboard)/handwired/dactyl_manuform/5x6/keymaps/$(user)
	-cd $(qmk_git) ;\
	$(MAKE) handwired/dactyl_manuform/5x6:bs0c:avrdude
	-rm -r $(qmk_keyboard)/handwired/dactyl_manuform/5x6/keymaps/$(user)

.PHONY: atreus
atreus:
	-ln -s $(source)/$@ $(qmk_keyboard)/handwired/$@60
	-cd $(qmk_git) ;\
	$(MAKE) handwired/$@60:bs0c:avrdude
	-rm -r $(qmk_keyboard)/handwired/$@60

.PHONY: check
check:
	@ echo -n "check git: "
	@ git --version
	@ echo -n "check qmk: "
	@ $(qmk) --version
	@ echo -n "check pip: "
	@ pip --version

.PHONY: clean
clean:
	-rm -r $(qmk_keyboard)/keebio/iris/keymaps/$(user)
	-rm -r $(qmk_keyboard)/handwired/dactyl_manuform/5x6/keymaps/$(user)
	-rm -r $(qmk_keyboard)/handwired/atreus60
